﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdersAssessment.Models;
using System.Data;
using PagedList;
using PagedList.Mvc;

namespace OrdersAssessment.ViewModel
{
    public class ViewModel
    {
        public IPagedList<SupplierList> Suppliers { get; set; }
        public IPagedList<OrderList> Orders { get; set; }
        public IPagedList<ProductLineList> ProductLines { get; set; }


        public List<SupplierList> AllSuppliers { get; set; }
        public List<ProductLineList> AllProductLinesList { get; set; }
        public List<OrderList> AllOrdersList { get; set; }
    }
}