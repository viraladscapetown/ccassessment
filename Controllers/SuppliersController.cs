﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersAssessment.Models;
using PagedList;
using PagedList.Mvc;

namespace OrdersAssessment.Controllers
{
    public class SuppliersController : Controller
    {
        public ActionResult Index(int? page)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            Suppliers suppliers = new Suppliers();
            suppliers.createdBy = (int)Session["user_id"];
                        
            var viewModel = new ViewModel.ViewModel
            {
                Suppliers = suppliers.getAllSuppliers(null).ToList().ToPagedList(page ?? 1,3)
            };
            return View(viewModel);
        }

        //Add Supplier
        [HttpPost]
        public ActionResult AddSupplier()
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Request["supplier_name"] != null)
            {
                Suppliers suppliersModel = new Suppliers();
                suppliersModel.supplierName = Request["supplier_name"].ToString();
                suppliersModel.createdBy = (int)Session["user_id"];
                if(suppliersModel.hasCreatedSupplier())
                {
                    return RedirectToAction("Index", new { @status = "success", @message = "Supplier successfully created" });
                }
                else
                {
                    return RedirectToAction("Index",new { @status = "danger", @message = "Error failed adding new supplier"});
                }
            }
            else
            {
                return RedirectToAction("Index", new { @status = "danger", @message = "Error failed adding new supplier"});
            }            
           
        }

       [Route("suppliers/edit/{id:regex(\\d)}")]
        public ActionResult Edit(int? Id)
        {
            if(!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }
            Suppliers suppliers = new Suppliers();
            suppliers.Id = (int)Id;
            suppliers.createdBy = (int)Session["user_id"];
            var viewModel = new ViewModel.ViewModel
            {
                AllSuppliers = suppliers.getAllSuppliers(Id)
            };

            if(viewModel.AllSuppliers.Count == 0)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }



        //Edit Supplier
        [HttpPost]
        public ActionResult UpdateSupplier(int Id)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }
            var result = false;
            if (Request["supplier_name"] != null)
            {
                Suppliers suppliersModel = new Suppliers();
                suppliersModel.supplierName = Request["supplier_name"].ToString();
                suppliersModel.createdBy = (int)Session["user_id"];
                suppliersModel.Id = Id;
                if(suppliersModel.hasUpdatedSupplier())
                {

                }                
            }
            else
            {
                result = false;
            }

            return RedirectToAction("Index");
        }
    }
}