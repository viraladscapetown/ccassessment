﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersAssessment.Models;
using OrdersAssessment.ViewModel;
using PagedList;
using PagedList.Mvc;

namespace OrdersAssessment.Controllers
{
    public class ProductLineController : Controller
    {

        

        // GET: ProductLine
        public ActionResult Index(int? page)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            ProductLines productLine = new ProductLines();
            productLine.createdBy = (int)Session["user_id"];
            var viewModel = new ViewModel.ViewModel
            {
                ProductLines = productLine.getAllProductLines(null).ToList().ToPagedList(page ?? 1,3)
            };
            return View(viewModel);
        }

        //Add product line
        [HttpPost]
        public ActionResult AddProductLine()
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Request["product_line"] != null)
            {
                ProductLines productLine = new ProductLines();
                productLine.product_line_name = Request["product_line"].ToString();
                productLine.createdBy = (int)Session["user_id"];
                if(productLine.hasCreatedProductLine())
                {
                    //created_product_line
                    return RedirectToAction("Index", new { @status = "success", @message = "Product Line successfully created" });
                }
            }
            else
            {
                return RedirectToAction("Index", new { @status = "danger", @message = "Error failed adding new product line" });
            }

            return RedirectToAction("Index", new { @status = "danger", @message = "Error failed adding new product line" });

        }

        public ActionResult Edit(int? Id)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }

            ProductLines productLine = new ProductLines();
            productLine.Id = (int)Id;
            productLine.createdBy = (int)Session["user_id"];
            var viewModel = new ViewModel.ViewModel
            {
                AllProductLinesList = productLine.getAllProductLines(Id)
            };
            if (viewModel.AllProductLinesList.Count == 0)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        //Edit Product line
        [HttpPost]
        public ActionResult UpdateProductLine(int Id)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }
            var result = false;
            if (Request["product_line"] != null)
            {
                ProductLines productLine = new ProductLines();
                productLine.product_line_name = Request["product_line"].ToString();
                productLine.createdBy = (int)Session["user_id"];
                productLine.Id = Id;
                if (productLine.hasUpdatedProductLine())
                {

                }
            }
            else
            {
                result = false;
            }

            return RedirectToAction("Index");
        }
    }
}