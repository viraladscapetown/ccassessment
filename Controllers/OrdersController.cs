﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersAssessment.Models;
using System.Data;
using PagedList;
using PagedList.Mvc;

namespace OrdersAssessment.Controllers
{
    public class OrdersController : Controller
    {
        public ActionResult Index(int? page)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            Suppliers suppliers = new Suppliers();
            suppliers.createdBy = (int)Session["user_id"];

            Orders orders = new Orders();
            orders.createdBy = (int)Session["user_id"];

            ProductLines productLine = new ProductLines();
            productLine.createdBy = (int)Session["user_id"];

            var viewModel = new ViewModel.ViewModel
            {
                AllSuppliers = suppliers.getAllSuppliers(null),
                AllProductLinesList = productLine.getAllProductLines(null),
                Orders = orders.getAllOrders(null).ToList().ToPagedList(page ?? 1,3)
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult addOrder()
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            Orders orders = new Orders();
            orders.supplierId = Convert.ToInt32(Request["supplierId"]);
            orders.productionLineId = Convert.ToInt32(Request["product_line"]);
            orders.createdBy = (int)Session["user_id"];
            if(orders.hasCreatedOrder())
            {
                return RedirectToAction("Index", new { @status = "success", @message = "Order successfully created" });
            }

            return RedirectToAction("Index", new { @status = "danger", @message = "Error failed adding new order" });
        }


        public ActionResult Edit(int? Id)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }
            Suppliers suppliers = new Suppliers();
            suppliers.createdBy = (int)Session["user_id"];

            Orders orders = new Orders();
            orders.createdBy = (int)Session["user_id"];

            ProductLines productLine = new ProductLines();
            productLine.createdBy = (int)Session["user_id"];

            var viewModel = new ViewModel.ViewModel
            {
                AllSuppliers = suppliers.getAllSuppliers(null),
                AllProductLinesList = productLine.getAllProductLines(null),
                AllOrdersList = orders.getAllOrders(Id)
            };
            if (viewModel.AllOrdersList.Count == 0)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }


        //Edit Product line
        [HttpPost]
        public ActionResult UpdateOrders(int Id)
        {
            if (Session["user_id"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (Id.Equals(null))
            {
                return RedirectToAction("Index");
            }
            var result = false;
            Orders orders = new Orders();
            orders.supplierId = Convert.ToInt32(Request["supplierId"]);
            orders.productionLineId = Convert.ToInt32(Request["product_line"]);
            orders.Id = Convert.ToInt32(Id);
            orders.createdBy = (int)Session["user_id"];
            if(orders.hasUpdatedOrder())
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return RedirectToAction("Index");
        }
    }
}