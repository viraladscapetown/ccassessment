﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersAssessment.Models;

namespace OrdersAssessment.Controllers
{
    public class UsersController : Controller
    {        
            // Login Page
            public ActionResult Login()
            {                
                return View();
            }

            //Login Post
            [HttpPost]
            public ActionResult LoginUser()
            {
                Users usersModel = new Users();
                usersModel.username = Request["username"];
                usersModel.password = Request["password"];
                var user_id = usersModel.isValidUser();
                    if (user_id > 0)
                    {
                        Session["user_id"] = user_id;
                        return RedirectToAction("Index", "Orders");
                    }
                    else
                    {
                        return RedirectToAction("Login", new { @status = "danger", @message = "Please make sure you are using the correct username or password" });                
                    }
            }

            //Logout
            public ActionResult LogoutUser()
            {
                Session.Clear();
                Session.Abandon();                
                return RedirectToAction("Login");
            }        
    }
}