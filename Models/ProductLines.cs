﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace OrdersAssessment.Models
{
    public class ProductLines
    {
        [Required(ErrorMessage = "Product line required")]
        public string product_line_name { get; set; }
        public int createdBy { get; set; }
        [Key]
        public int Id { get; set; }

        public ProductLines()
        {
            //constructor
        }
        public bool hasCreatedProductLine()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "insert into tbl_product_line(product_line,date_created,is_active,created_by)VALUES(@product_line,GETDATE(),1,@user_id)";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@product_line", product_line_name);
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }catch(Exception e)
            {
                status = false;
            }
            return status;
        }
        public bool hasUpdatedProductLine()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "update tbl_product_line set product_line = @product_line where ID = @ID and created_by = @user_id";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@product_line", product_line_name);
                    command.Parameters.AddWithValue("@ID", Id);
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }catch(Exception e)
            {
                status = false;
            }
            return status;
        }
        public List<ProductLineList> getAllProductLines(int? Id)
        {
            List<ProductLineList> ls = new List<ProductLineList>();
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    string query = "select * from tbl_product_line where is_active = 1 and created_by = @user_id";
                    if (Id != null)
                    {
                        query = query + " and ID = @ID";
                        command.Parameters.AddWithValue("@ID", Id);
                    }
                    command.CommandText = query;
                    command.Connection = conn;
                    command.Parameters.AddWithValue("@user_id", createdBy);

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ProductLineList sp = new ProductLineList(Convert.ToInt32(reader["ID"]), Convert.ToString(reader["product_line"]),
                             Convert.ToString(reader["date_created"]));
                        ls.Add(sp);
                    }
                    reader.Close();
                }
            }
            catch(Exception e)
            {
                //handle error
            }
            return ls;
        }
    }
    public class ProductLineList
    {
        public string name { get; set; }
        public int id { get; set; }
        public string date_created { get; set; }

        public ProductLineList(int id, string name, string date_created)
        {
            this.id = id;
            this.name = name;
            this.date_created = date_created;
        }
    }
}