﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel.DataAnnotations;

namespace OrdersAssessment.Models
{
    public class Users
    {
        
        [Required(ErrorMessage = "Username required")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password required")]
        public string password { get; set; }

        [Key]
        public int id { get; set; }

        public Users()
        {
            //constructor
        }

        //check wheter the username or password exist in the db
        public int isValidUser()
        {
            var user_id = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
            {
                conn.Open();
                string query = "select * from tbl_users where username =  @username and password = @password and is_active = 1";
                SqlCommand command = new SqlCommand(query,conn);
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@password", PasswordEncryption.MD5Hash(password));
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    user_id = Convert.ToInt32(reader["ID"]);
                }
            }
            return user_id;
        }
    }
}