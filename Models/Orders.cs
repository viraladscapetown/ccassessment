﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace OrdersAssessment.Models
{
    public class Orders
    {
        [Required(ErrorMessage = "Supplier required")]
        public int supplierId { get; set; }
        [Required(ErrorMessage = "Product line required")]
        public int productionLineId { get; set; }
        public int createdBy { get; set; }
        public int Id { get; set; }

        public Orders()
        {
            //constructor
        }
        public bool hasCreatedOrder()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "insert into tbl_orders(supplier_id,product_line_id,date_created,is_active,created_by)VALUES(@supplier_id,@product_line_id,GETDATE(),1,@user_id)";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@supplier_id", supplierId);
                    command.Parameters.AddWithValue("@product_line_id", productionLineId);
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }
            catch(Exception e)
            {
                status = false;
            }
            return status;
        }
        public bool hasUpdatedOrder()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "update tbl_orders set product_line_id = @product_line_id,supplier_id = @supplier_id where ID = @ID and created_by = @user_id";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@supplier_id", supplierId);
                    command.Parameters.AddWithValue("@product_line_id", productionLineId);
                    command.Parameters.AddWithValue("@ID", Id);
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }catch(Exception e)
            {
                status = false;
            }
            
            return status;
        }
        public List<OrderList> getAllOrders(int? Id)
        {
            List<OrderList> ls = new List<OrderList>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    string query = "select tbl_suppliers.suppliers_name,tbl_product_line.product_line,tbl_orders.* from tbl_orders" +
                        " inner join tbl_suppliers on " +
                        " tbl_orders.supplier_id = tbl_suppliers.ID " +
                        " inner join tbl_product_line on " +
                        " tbl_orders.product_line_id = tbl_product_line.ID  where tbl_orders.is_active = 1 and tbl_orders.created_by = @user_id";
                    if (Id != null)
                    {
                        query = query + " and tbl_orders.ID = @ID";
                        command.Parameters.AddWithValue("@ID", Id);
                    }
                    command.CommandText = query;
                    command.Connection = conn;
                    command.Parameters.AddWithValue("@user_id", createdBy);

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        OrderList sp = new OrderList(Convert.ToInt32(reader["ID"]), Convert.ToString(reader["suppliers_name"]), Convert.ToString(reader["product_line"]),
                             Convert.ToString(reader["date_created"]), Convert.ToInt32(reader["supplier_id"]), Convert.ToInt32(reader["product_line_id"]));
                        ls.Add(sp);
                    }
                    reader.Close();
                }
            }
            catch(Exception e)
            {

            }
            
            return ls;
        }
    }
    public class OrderList
    {
        public string suppliers_name { get; set; }
        public int supplierId { get; set; }
        public string product_line { get; set; }
        public int productLineId { get; set; }
        public int id { get; set; }
        public string date_created { get; set; }


        public OrderList(int id, string suppliers_name,string product_line, string date_created,int supplierId,int productLineId)
        {
            this.id = id;
            this.suppliers_name = suppliers_name;
            this.product_line = product_line;
            this.date_created = date_created;
            this.supplierId = supplierId;
            this.productLineId = productLineId;
        }
    }
}