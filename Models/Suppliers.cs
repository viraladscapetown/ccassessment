﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace OrdersAssessment.Models
{
    public class Suppliers
    {
        [Required(ErrorMessage = "Supplier name required")]
        public string supplierName { get; set; }

        public int createdBy { get; set; }

        [Key]
        public int Id { get; set; }

        public Suppliers()
        {
            //constructor
        }
        public bool hasCreatedSupplier()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "insert into tbl_suppliers(suppliers_name,date_created,is_active,created_by)VALUES(@supplier_name,GETDATE(),1,@user_id)";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@supplier_name", supplierName);
                    command.Parameters.AddWithValue("@user_id", createdBy);

                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }
            catch (Exception e)
            {
                status = false;
            }
            return status;
        }
        public bool hasUpdatedSupplier()
        {
            bool status = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    string query = "update tbl_suppliers set suppliers_name = @supplier_name where ID = @ID and created_by = @user_id";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.Parameters.AddWithValue("@supplier_name", supplierName);
                    command.Parameters.AddWithValue("@ID", Id);
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    int result = command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        status = true;
                    }
                }
            }
            catch(Exception e)
            {
                status = false;
            }
            return status;
        }

        public List<SupplierList> getAllSuppliers(int? Id)
        {
            List<SupplierList> ls = new List<SupplierList>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionStr"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    string query = "select * from tbl_suppliers where is_active = 1 and created_by = @user_id";
                    if (Id != null)
                    {
                        query = query + " and ID = @ID";
                        command.Parameters.AddWithValue("@ID", Id);
                    }
                    command.CommandText = query;
                    command.Connection = conn;
                    command.Parameters.AddWithValue("@user_id", createdBy);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        SupplierList sp = new SupplierList(Convert.ToInt32(reader["ID"]), Convert.ToString(reader["suppliers_name"]),
                             Convert.ToString(reader["date_created"]));
                        ls.Add(sp);
                    }
                    reader.Close();

                }
            }
            catch(Exception e)
            {
                //handle error 
            }
            return ls;
        }
    }
    public class SupplierList
    {
        public string name { get; set; }
        public int id { get; set; }
        public string date_created { get; set; }

        public SupplierList(int id,string name,string date_created)
        {
            this.id = id;
            this.name = name;
            this.date_created = date_created;
        }
    }
}